# TSMA 2023/24 : Music genre classification

lien git : https://gitlab.emi.u-bordeaux.fr/selebreton/tsma_classification.git

## Rendu 1 : Random + KNN avec MFCCs

Ce premier rendu consistait à soumettre 2 classification, une aléatoire et l'autre en utilisant un classifier knn entrainé avec les mfccs des morceaux.

### Random
Rendu assez simple ou malgré tout la chance ne m'a pas forcément sourie, j'ai obtenu un score de 0.11598. On espère ici environ 0.125 étant donné qu'il y a 8 classes.

### KNN et MFCCS

Pour ce rendu, j'ai dû d'abord convertir les fichiers .mp3 en 
.wav puis ensuite calculé les MFCCs des 8000 morceaux.
Ce processus à été assez long d'autant plus que mon ordi (assez peu performant) à planté plusieures fois et j'ai du donc recommencer les calculs à multiples reprises. Cela a du me prendre environ 8h juste pour la conversion et le calculs des MFCCs.

Ensuite en ce qui concerne la classification, j'ai eu plusieures erreures qui ont faussées le score. Déjà, j'avais oublié d'enlever la colone 'track_id' dans mes csv de train et de test. Si dans un contexte normal, utiliser un nom de morceaux pour le classifieur peut être pertinent, dans notre cas, étant donné que les morceaux n'étaient pas mélangés le classifieur se basait uniquement sur l'id du morceaux et j'ai donc obtenu un score de 0.67927.

Une fois cette erreur réglée, j'en ai constaté une autre, les données des MFCCs dans mon csv n'était pas attribuée au bon morceaux (par exemple le titre 000002 avait les mfcc du titre 110728) ce qui me donnait donc un score de classification de 
0.10892 soit pire que la classification aléatoire. Cela venait du fait que l'appel à la fonction ```os.walk()``` pour obtenir les fichiers ne les renvoies pas dans l'ordre. J'ai donc trié mon dictionnaire contenant les MFCCs par rapport au ids des morceaux pour corriger ce problème.

```python
in create_csv.py

myKeys = list(dico.keys())
myKeys.sort()
sorted_dict = {i: dico[i] for i in myKeys}

```

De plus dans mon classifier j'ai testé mon modèle avec différent K afin de voir quel est le meilleur nombre de voisin à prendre en compte.

```python
def best_model_search(train, test, train_label, test_label, n=10, patience=3):
    k = 1
    best_acc = 0
    best_k = 0
    best_model = None
    p = patience
    while k <n and p >0 :
        knn = KNeighborsClassifier(n_neighbors=k,metric='euclidean')
        knn.fit(train, train_label)
        y_pred = knn.predict(test)
        cm = confusion_matrix(test_label, y_pred)
        acc = np.sum(np.diag(cm))/np.sum(cm)
        print('k: ',k, '{:.2%}'.format(acc))
        if acc > best_acc:
            best_acc = acc
            best_model = knn
            best_k = k 
            p = patience + 1
        p = p -1
        k = k+1
    return best_model,best_k, best_acc
```

Résultat :

    k:  1 28.79%
    k:  2 27.91%
    k:  3 28.54%
    k:  4 28.16%
    k:  5 30.91%
    k:  6 29.91%
    k:  7 32.17%
    k:  8 30.91%
    k:  9 32.42%
    k:  10 31.29%
    k:  11 31.54%
    k:  12 31.41%
    k:  13 31.41%
    k:  14 33.79%
    k:  15 33.42%
    k:  16 33.92%
    k:  17 33.54%
    k:  18 33.17%
    k:  19 33.92%
    k:  20 33.42%
    k:  21 33.79%
    k:  22 33.54%
    k:  23 33.29%
    k:  24 32.54%
    k:  25 32.67%
    k:  26 32.54%

Aprèes plusieurs test j'ai observé que généralement j'ai observe le meilleur résultat avec environ k=10. C'est donc le k que j'ai choisi pour mon rendu final.

J'ai don obtenu un score final de 0.32425 avec KNN.  
<br>
*** 
<br>

## Rendu 2 librosa features et Catboost

Ce rendu consistait à utiliser de nouvelles caractéristique pour les classification en s'appuyant sur les features de la librairie "librosa", ainsi ue l'utilisation d'un nouveau classifier plus performant pour les musiques Catboost.

### Librosa

J'avais créer un script python afin d'extraire les features pour les 8000 morceaux. Malheureusement mon ordi assez vieux et trop peu perfomant plantait à chaque fois au bout d'environ 1h d'extraction (durant laquelle j'avais pu parcourir à peine 1200 morceaux ). Ayant essayé plusieurs fois en réduisant le nombre de features à extraire à chaque fois pour finir par une seule feature à la fin, on ordi plantait encore. J'ai donc emprunté les csvs de features du groupe de Benoit et Aurélie afin de pouvoir avancer et de réalier les classifications.

### Catboost 

Pour ce rendu après avoir esayer catboost avec différent parametres j'ai finalment choisi les suivant :

```python
model = CatBoostClassifier(iterations=4000,
                          learning_rate=0.001,
                          depth=7,
                          eval_metric="Accuracy")
```

Avec lequel j'ai obtenu en utilisant les features de librosa un score final de 0.43116 contre 0.36459 en utilisant catboost avec les MFCCs.  
<br>
*** 
<br>

## Rendu 3 : Deep learning + VGGish

Ce rendu consistait à utliser les données VGGish avec un modèle de deep learning afin d'obtenir le meilleur résultat possible.


### VGGish

Les données ont étées récupérées gràce au collab présent sur moodle.


### Deep learning 

Pour le premier rendu, j'ai obtenu un score de 0.05395 cela venait du fait que les genres allaient de 0 à 7 au lieu de 1 à 8. j'ai donc corriger cela en ajoutant +1 aux prédictions du modèle.

J'ai effectuer des test avec différent modèles
le premier modèle était le suivant :

    Model: "sequential_9"
    _________________________________________________________________
    Layer (type)                Output Shape              Param #   
    =================================================================
    dense_43 (Dense)            (None, 512)               2032128   
                                                                    
    dropout_24 (Dropout)        (None, 512)               0         
                                                                    
    dense_44 (Dense)            (None, 256)               131328    
                                                                    
    dropout_25 (Dropout)        (None, 256)               0         
                                                                    
    dense_45 (Dense)            (None, 128)               32896     
                                                                    
    dropout_26 (Dropout)        (None, 128)               0         
                                                                    
    dense_46 (Dense)            (None, 64)                8256      
                                                                    
    dropout_27 (Dropout)        (None, 64)                0         
                                                                    
    dense_47 (Dense)            (None, 8)                 520       
                                                                    
    =================================================================
    Total params: 2,205,128
    Trainable params: 2,205,128
    Non-trainable params: 0
    _________________________________________________________________

Le score obtenu par ce modèle a été de 0.6238.

J'ai ensuite enlever les layers de Dropout car je me suis dit que dans tout les cas il n'y a pas de raison que le modèle overfit.

Le nouveau modèle était donc :


    Model: "sequential_10"
    _________________________________________________________________
    Layer (type)                Output Shape              Param #   
    =================================================================
    dense_48 (Dense)            (None, 512)               2032128   
                                                                    
    dense_49 (Dense)            (None, 256)               131328    
                                                                    
    dense_50 (Dense)            (None, 128)               32896     
                                                                    
    dense_51 (Dense)            (None, 64)                8256      
                                                                    
    dense_52 (Dense)            (None, 8)                 520       
                                                                    
    =================================================================
    Total params: 2,205,128
    Trainable params: 2,205,128
    Non-trainable params: 0
    _________________________________________________________________

Il a obtenu un score de 0.63136, soit +1%.

Enfin, pour le model final j'ai décide d'enlever le premier layer d'input de taille 512 et de commencé directment par le 256.

    Model: "sequential_8"
    _________________________________________________________________
    Layer (type)                Output Shape              Param #   
    =================================================================
    dense_39 (Dense)            (None, 256)               1016064   
                                                                    
    dense_40 (Dense)            (None, 128)               32896     
                                                                    
    dense_41 (Dense)            (None, 64)                8256      
                                                                    
    dense_42 (Dense)            (None, 8)                 520       
                                                                    
    =================================================================
    Total params: 1,057,736
    Trainable params: 1,057,736
    Non-trainable params: 0




J'ai donc obtenu un score final de : 0.063691, une amèlioration très légère. (~ +0.5%).

Pour chaque rendu de deep learning j'ai pris un nombre d'epoch = 10 mais j'aurais surment du prendre 4 ou 5 car c'est la que l'accuracy est la meilleure.