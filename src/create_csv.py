import csv
import os

for dirname, _, filenames in os.walk('test_mfcc'):
    for filename in filenames:
        continue
        
        
dico = {}
# csv data
for filename in filenames : 
    result  = filename.split('/')[-1]
    t_id = result.split('.')[0] 
    with open(dirname+"/"+filename) as f:
        lines=f.readlines()
        for x in lines:
            result = x.split(' ')[:-1]
            for d in result:
                d = float(d)
            dico[t_id] = result




with open('csv', newline='') as csvfile:
    spamreader = csv.reader(csvfile, delimiter=' ', quotechar='|')
    for row in spamreader:
        
        print(row[0].split(",")[0])
    
myKeys = list(dico.keys())
myKeys.sort()
sorted_dict = {i: dico[i] for i in myKeys}


header = ['track_id','0','1','2','3','4','5','6','7','8','9','10','11','12','13','14','15','16','17','18','19','20','21']
with open("test_csv.csv", 'w') as csv_file:  
    writer = csv.writer(csv_file)
    writer.writerow(header)
    for key, value in sorted_dict.items():
        data  = []
        data.append(key)
        for v in value :
            data.append(v)
        writer.writerow(data)
